#!/usr/bin/env python3

from colorama import init, Fore, Style
from tabulate import tabulate


class Hole:
    def __init__(self, blow, draw):
        self.blow = blow
        self.draw = draw


class Harmonica:
    """
    Initialize with comma-separated string list of notes;
    alternate between blow and draw.

    State is saved as numbers for each hole.
    0: no note played
    1: slight blow
    2: strong blow
    negative: draw
    """

    def __init__(self, layout):
        self.layout = []

        # TODO: cover edge cases for input string

        layout = layout.split(',')
        for i in range(0, len(layout), 2):
            self.layout.append(Hole(layout[i], layout[i + 1]))

        self.state = [0 for i in range(len(self.layout))]

    def __str__(self):
        table = [[hole.blow for hole in self.layout],
                 [Style.BRIGHT + str(i) + Style.RESET_ALL for i in range(1, len(self.layout)+1)],
                 [hole.draw for hole in self.layout]]
        # Add accents accordingly
        for i in range(len(self.layout)):
            if self.state[i] == 1:
                table[0][i] = Style.BRIGHT + Fore.YELLOW + table[0][i] + Style.RESET_ALL
            elif self.state[i] == 2:
                table[0][i] = Style.BRIGHT + Fore.RED + table[0][i] + Style.RESET_ALL
            elif self.state[i] == -1:
                table[2][i] = Style.BRIGHT + Fore.YELLOW + table[2][i] + Style.RESET_ALL
            elif self.state[i] == -2:
                table[2][i] = Style.BRIGHT + Fore.RED + table[2][i] + Style.RESET_ALL

        return tabulate(table, tablefmt='grid')


if __name__ == '__main__':
    # Initialize colorama
    init()

    monica = Harmonica('C4,D4,E4,G4,G4,A4,C5,D5,E5,F5,G5,A5,C6,H5,E6,D6,G6,F6,C7,A6')
    monica.state = [2, -1, 0, 0, 0, 1, 0, 0, 0, 0, 0]
    print(monica)
